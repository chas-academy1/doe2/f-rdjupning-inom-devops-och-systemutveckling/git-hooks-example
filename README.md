# git hook example

This repo shows an example of a git `pre-commit` hook that checks code quality
(PEP8, linting) and runs unittests for the repository before a commit can be
made.

## Install the hook

```bash

ln git-pre-commit-hook.sh .git/hooks/pre-commit
```
