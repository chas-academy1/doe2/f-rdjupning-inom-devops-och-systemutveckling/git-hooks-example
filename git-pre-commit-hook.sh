#/usr/bin/env bash
#
# This is a git pre-commit hook.
#
# To install it in the current repo, do something like:
#
#     ln git-pre-commit-hook.sh .git/hooks/pre-commit
#
# This hook checks for PEP8 compliancy, lints using pylint and runs tests in
# the src/tests directory with pytest.

echo "git pre-commit hook"

echo "    checking PEP8 compliancy"

for FILE in $(find src/ -name '*.py')
do
    autopep8 --exit-code "$FILE" >/dev/null
    result=$?
    if [[ $result -ne 0 ]]
    then
        echo "${FILE} isn't PEP8 compliant!"
        echo -n "Fixing with autopep8..."
        autopep8 -i "$FILE"
        echo "done!"
        echo "Staging the changes..."
        git add "$FILE"
        echo "Commit again" 
        exit $result
    fi
done

echo "    linting with pylint"
pylint src/
result=$?
if [[ $result -ne 0 ]]
then
    echo "Quitting..."
    exit $result
fi

echo "    running unittests"
pytest src/
result=$?
if [[ $result -ne 0 ]]
then
    echo "Quitting..."
fi
